<?php header("Content-Type: text/html"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pixelator</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZMAAAGTCAYAAADtHP9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7d17nN1nXeDxT+5Jb5l0Ai1YLGhlZb1NNFvW1XUtTFsELEpBQVz4AQUB8bLKellFX+ouu7LeUQRp2hcLlku5gwhNKC4oum0lEyilDaUtbaE0yaSTpLlNkpn948yvmSZzznnOOb/L83t+n/frNX+0PDN5XnR+r0++z3NmDihGy4A3Az9X90akiGTA1cDymvchNcIy4K+BeWAOgyJBJyQn6DwX12BQpJ4WhyT/MChqu4yTIck/DIrUxVIhMShqu4zTQ2JQpC6WA1tY+oHJP07QebCktsjoHpL8YwsGRQJ6TyROKGqrjP4hcUKRFoRMJE4oapuM8JA4oaj1BplInFDUFhmDh8QJRa3VdyKZmJiYHx8fd0JRm2T0CMn4+Pj8xMSEE4q0oO9EsmnTpvnp6en5qamp+Y0bNzqhqA0yeoRkbGxs/uabb56fmZmZv/jii51Q1HrBIckZFLVARkBIcgZFbTdwSAyKWiBjgJAYFLXd0CExKEpYxhAhMShqq5FDYlCUoIwRQmJQ1DaFhcSgKCEZBYTEoKgtCg+JQVECMgoMiUFR6koLiUFRg2WUEBKDolSVHhKDogbKKDEkBkWpqSwkOYOiBsioICQ5g6KmqzwkOYOiiGVUGJKcQVFT1RaSnEFRhDJqCEnOoKhpag9JzqAoIhk1hiRnUNQU0YQkZ1AUgYwIQpIzKIpddCHJGRTVKCOikOQMimIVbUhyBkU1yIgwJDmDothEH5KcQVGFMiIOSc6gKBaNCUnOoKgCGQ0ISc6gqG6NC0nOoKhEGQ0KSc6gqC6NDUnOoKgEGQ0MSc6gqGqND0nOoKhAGQ0OSc6gqCrJhCRnUFSAjARCkjMoKltyIckZFI0gI6GQ5AyKypJsSHIGRUPISDAkOYOioiUfkpxB0QAyEg5JzqCoKK0JSc6gKEBGC0KSMygaVetCkjMo6iGjRSHJGRQNq7UhyRkULSGjhSHJGRQNqvUhyRkULZLR4pDkDIpCGZJTGBRhSB7FoKgfQ9KFQWm1DENyGoOibgxJHwallTIMSVcGRacyJIEMSqtkGJK+DIpyhmRABqUVMgxJMIMiQzIkg5K0DEMyMIPSXoZkRAYlSRmGZGgGpX0MSUEMSlIyDMnIDEp7GJKCGZQkZBiSwhiU9BmSkhiURsswJIUzKOkyJCUzKI2UYUhKY1DSY0gqYlAaJcOQlM6gpMOQVMygNEKGIamMQWk+Q1KTwKC8qvpvCQEvxZBUzqA0lyGpmUGJkiGpkUFpHkMSCYMSFUMSAYPSHIYkMgYlCoYkIgYlfoYkUgalVj1DsmHDBkNSA4MSL0MSOYNSC0MSMYMSH0PSEAalUoakAQxKPAxJwxiUShiSBjEo9TMkDWVQSmVIGsig1MeQNJxBKYUhaTCDUj1DkgiDUihDkgCDUh1DkhiDUghDkhCDUj5DkiiDMhJDkiCDUh5DkjiDMhRDkjCDUjxD0hIGZSCGpAUMSnEMScsYlCCGpEUMyugMSUsZlJ4MSQsZlOEZkpYzKEsyJC1mUAZnSDQ/P29QTmFIZFAGYEj0KAYFMCRaxKD0Z0i0pJYHxZDoNAalO0OinloaFEOirgzK6QyJgrQsKIZEfRmUkwyJBtKSoBgSBTMohkRDSjwohkQDa3NQDIlGkmhQDImG1sagGBIVIrGgGBKNrE1BMSQqVCJBMSQqTBuCYkhUioYHxZCocCkHxZCoVA0NiiFRaVIMiiFRJRoWFEOi0qUUFEOiSjUkKIZElUkhKIZEtYg8KIZElasjKMsK/DpvpscDu2nTJrZt28a5555b0B8pnbRjxw4mJyfZs2dPtyXzwGuAt1S3K14KXE2XB3bDhg3ccMMNbN68ucItqS327dvHZZddxk033dRr2bXAVXT+wjWSImJiSBSFyIJiSFS7KoMyakwMiaISSVAMiaJRVVBGiYkhUZRqDoohUXSqCMqwMTEkilpNQTEkilbZQRkmJoZEjVBxUAyJoldmUAaNiSFRo1QUFEOixigrKIPExJCokUoOiiFR45QRlNCYGBI1WklBMSRqrKKDEhITQ6IkFBwUQ6LGKzIo/WJiSJSUgoJiSJSMooLSKyaGREkaMSiGRMkpIijdYmJIlLQhg2JIlKxRg7JUTAyJWmHAoBgSJW+UoJwaE0OiVgkMyoeA52BI1ALDBmVxTAyJWikgKF0ZEqVomKDkMVkOvA14WbfP2rx5M1u3bmVsbKyg7UrxmJqaYnJykunp6eDPGR8fZ9u2bUxMTJS4M6keMzMzXHrppdxyyy29ll0DvAKYW04nKH9Fj5Bs2rSJT37yk4ZEyZqYmOBTn/oUGzduDFq/YcMGPvGJTxgSJWtsbIxt27Zx8cUX91r2MhbuEpfRmUiu6rZyYmKCT3/604ZErTA1NcVTn/pUZmdnu65Zt24dn/vc5wyJWmFmZoZLLrmEqampXsuuXg58is57VC/pnnvu4c477yx6f1KU3vSmN/UMCcDhw4d54xvfWNGOpHrdc8893Hfffb2WzAH/uBx4Nz1+EGVmZoZnPOMZ/aokNd7LX/5yrrnmmqC173rXu3jJS15S8o6kegXcJc4BLwfevvjVXC8A3gmsWOozxsbG2Lp1q69aUZIGCcliL3zhC7nuuutK2JFUr8CQvAx4O5z+cyYGRa0zbEhyBkWpGTQkcPoPYb0b+Fm63KEEvlRMaox+IVnHOM/hGtYx3nWNR15KyTAhge6/m8sJRcnrF5K1bOBn+BiP4wd4kC9wHT/GIbr/HIoTippu2JBA798abFCUrP4hGVsIyfc/8u8e5ItcxzMNipI0Skig//uZGBQlJyQkL+SjjwpJbhdf5DqeZVCUlFFDAmHvtGhQlIyQkLyAjywZklwnKM/msEFRAooICYS/B7xBUeOFheTDPUOS6wTlxw2KGq2okEB4TMCgqMHCQvIhzmdT8Nfcxa28iysMihqpyJDAYDEBg6IGCgnJT/PBgUKS28WtvJvnGBQ1StEhgcFjAgZFDRIWkg8MFZJcJyg/YVDUCGWEBIaLCRgUNUBISH6K93M+o//2313cynt4rkFR1MoKCQwfEzAoilhYSN5XSEhynaBcaVAUpTJDAqPFBAyKIhQSkudzfaEhye3iS7yX53KYvV3XGBRVreyQwOgxAYOiiISE5Hm8p5SQ5HbzJd7L8wyKolBFSKCYmIBBUQT6h2Q9z+O9nM/3lb6XTlCeb1BUq6pCAsXFBAyKahQWkvdwXgUhye3mS1zPTxkU1aLKkECxMQGDohqEhORK3l1pSHK7uY33GRRVrOqQQPExAYOiCoWF5F2cx/dWuKtH6wTlBQZFlagjJFBOTMCgqAIhIXku19UaktxubuP9vNCgqFR1hQTKiwkYFJWoX0jWsJ7n8rdRhCS3m9v4AD9jUFSKOkMC5cYEDIpKEBaSd0QVktxuvswHeJFBUaHqDgmUHxMwKCpQSEh+kndwHt9T4a4Gs+eRoDzUdY1BUagYQgLVxAQMigoQFpL/w2MjDkluD1/mg/ysQdFIYgkJVBcTMCgaQVhI3t6IkOQ6QfnPBkVDiSkkUG1MwKBoCCEh+QmubVRIcnu4nQ/xYoOigcQWEqg+JmBQNIDwkHx3hbsqVicoLzEoChJjSKCemIBBUYD+ITmH5zQ8JLlOUDKOGBT1EGtIoL6YgEFRDyEhuYItSYQkt4fb+TAvMyhaUswhgXpjAgZFSwgLydVJhSS3hzsMik4Te0ig/piAQdEiISH5ca7msXxXhbuq1h7u4CO83KAIaEZIII6YgEERoSH5Gx6TcEhy09zBR3iFQWm5poQE4okJGJRWCwvJW1sRktzJoMx0XWNQ0tWkkEBcMQGD0kohIXk2b2lVSHLT3MFHeaVBaZmmhQTiiwkYlFYJCcmzeAuP4d9WuKu4TLOTjxmU1mhiSCDOmABkwBZg+VL/o0FJQ1hI3tzqkOQ6QXmVQUlcQEjmgVcDb61uV2FijQkYlKSFhOSZ/JUhWWSanfwdrzYoiWpySCDumIBBSVJISH6MvzQkS5hmJx/nNQYlMU0PCcQfEzAoSekfkrMXQvKUCnfVLNN8hY/z8wYlESmEBJoREzAoSQgLyZvYaEj62stX+DivNSgNl0pIoDkxAYPSaGEh+QtDMoBOUH7BoDRUSiGBZsUEDEojhYTkGfy5IRnCXr7C3/OLBqVhUgsJNC8mYFAaJSQkl/NnbOQ7K9xVWvZyJ5/gFznCvq5rDEo8UgwJNDMmYFAaISwkf2pICtAJyi8ZlMilGhJobkzAoEStX0hWczaX8yeGpEB7uZNP8ssGJVIphwSaHRMwKFEKCcll/DEb+TcV7qod9nInN/ArBiUyO3bs4OlPf3qyIYHmxwQMSlTCQvJHhqREe/mqQYnIjh07mJycZM+ePd2WND4kkEZMwKBEISwk/5txQ1K6TlB+laMGpVZtCQmkExMwKLUKCcmlvNGQVOghvsoNvM6g1KRNIYG0YgIGpRZhIflDxnlyhbsS5EH5NYNSsbaFBNKLCRiUSvUPyVlMGpJaPcRdbDUolWljSCDNmIBBqURISJ7O/zIkEXiIu9jGrxuUkrU1JJBuTMCglCosJG8wJBHpBOU3OMr+rmsMyvDaHBJIOyZgUEoREpKn8QbG+Y4Kd6UQD3EXn+I3DUrB2h4SSD8mYFAKFRaS/2FIItYJyn8zKAUxJB1tiAkYlEKEheQPONeQRO8h7uZGfsugjMiQnNSWmIBBGUlISC7h9w1Jg8xwNzfy2wZlSIbk0doUEzAoQwkPyUUV7kpF6ATl9QZlQIbkdG2LCRiUgYSE5Ef5PUPSYDPczaf5HYMSyJAsrY0xAYMSJCQk/4nfNSQJmOEe/oHfNSh9GJLuVtS9gZpMAfcCV7BEUI8cOcL111/P5OQkj3/84yvfXAzCQ/LtFe5KZVnLGOczwf38Myc4uuSaW2+9lZ07d3LllVdWvLs4GJLe2hoTMChdhYTkR/gdQ5KYk0H5F4NyCkPSX1uPuRbL8MjrEf1DciY/wuvZYEiSNcM9fIbf5ygHuq5p05GXIQljTDoyDEpgSH7bkLTADF8zKBiSQRiTkzJaHJSQkPxHfpsNfFuFu1Kd9vE1PsMftDYohmQwxuTRMloYlJCQ/DC/ZUhaaB9f47P899YFxZAMzpicLqNFQQkLyW8akhbrBOUNrQmKIRmOMVlaRguC0i8kqziTH+Y3DInYx718ljcwm3hQDMnwjEl3GQkHJTwkT6pwV4pZJyj/M9mgGJLRGJPeMhIMSkhIfohfMyQ6zT7u5R/5w+SCYkhGZ0z6y0goKGEh+a+MGRJ1sY97+SfemExQDEkxjEmYjASCEhaS1xkS9bWP+5IIiiEpjjEJl9HgoISE5D/wq4zxxOo2pUWa9yju4z4+xxuZ5eGua2IOiiEpVvO+g+uV0cCg9A/JGfygIdEQ9nMfn+OPGhcUQ1I8YzK4jAYFJSwkv2JINLROUP64MUExJOUwJsPJaEBQQkLy7/kvhkQj2899/DN/En1QDEl5jMnwMiIOSkhInsovM8aFFe5KKdvP/fwLfxptUAxJuYzJaDIiDEpYSH7JkKhwnaD8WXRBMSTlMyajy4goKGEh+UXWGxKVZD/38//482iCYkiqYUyKkRFBUEJCcjG/wHq+tdR9SPv5OjfxF7UHxZBUx5gUJ6PGoISF5LWGRJXpBOVNtQXFkFTLmBQro4aghITk3/HzhkSVO8DXuYm/rDwohqR6xqR4GRUGJSQkm3lN40LiN2Y6TgblYNc1RQbFkNTDZ7YcGRUEJSwkr2Y9Txjpz5FGdYCvczNvLj0ohqQ+xqQ8GSUGJSwkr+IcQ6JIHOAb3FJiUAxJvYxJuTJKCEr/kKwzJIpSJyh/XXhQDEn9jEn5MgoMSkhIfoBXGhJFqxOUt3KsoKAYkjgYk2pkFBCUkJB8P6/kHC4YYatS+Q7wDf6Vvxk5KIYkHsakOhkjBCUsJK8wJGqMAzwwUlAMSVyMSbUyhghKSEg2cZUhUeMc4AE+z9sGDoohiY8xqV7GAEHpF5KVrGMTLzMkaqyHeYDPsyU4KIYkTivq3kALTQH3AlewRMyPHDnC9ddfz+TkJK9//esDQ/It5e1WKtlqzmKc72AXX2KOY0uuufXWW9m5cycXXXSRIYmUk0l9XgpcTZcJZfXq1czOznb95E5IXsrZhkSJeJgH2M41HONQ1zV9nos54Crg2hK2pz6MSb0yehx5dbOSdUzwEkOi5DzMN5ni2p5B6cKJpGYec9VrCrgP+HECw76KMxYmkseVujGpDqs5i3O5iN09jryWkE8kV5e3M/VjTOrX8w5lsZWs4/t4MWcZEiXsZFBuCwnKPPAaDEntPOaKx/XA83ot+E5+kvP43oq2I9XrQb7A7Xyw37L3Ac+vYDvqw8kkDi8Ffp0+cZ/hHjbwJNZwdjW7kmryMA9yOx8ImUyeAjwI3FL+rtSLMalfz1d1LTbHcfbwZcb4NlZzVvk7k2rwMN/ki7yDYxwOWb4MeCadu8epUjemnoxJvYJDkpvjOLu5jTGexBqDosQc5EG+yN+GhiS3jM6doxNKjbwzqU/PkKxbt47Dh7s/UCtZy/fwQi/jlYyD7OKLXNfzZcF9ngt/zqRGTib16BmSDRs2cOONNzI3N8f27duX/AKdI6/bGeNCj7zUeAfZxa1c13MiedGLXsQ111zD+973Pg4dWjI4Tig1MibV6xuSG264gc2bN3PFFVdw//33BwTliQZFjdUJybv6huSd73wn559/Ppdffnm/oDwLg1I5j7mqFRySxa666iq2bNnS9YuuZA3fxU9zFucXuVepdAfZxZd4T1BIFgv8ZY+vAd5S1F7Vm5NJdYYKCRAwoZxgmttZ75GXGuQgu7iN9w4cEsAJJULGpBpDhyQXFpQ7WM+3GhRF7yC7hw5JzqDExZiUb+SQ5EKDcg4XspozR9mzVJpOSK7n+AghyRmUeHhnUq7CQrJYyB3KU7iSMzlvoK8rle0Qu7mN9xcSksW8Q6mfk0l5SgkJhE0oe9nJep7ghKJodELygcJDAk4oMTAm5SgtJLmwI6+vsJ4nsMqgqGaH2MOXSwpJzqDUy2Ou4pUeksX6HXmtYA3fyU9wJo8t5M+TBnWIPdzOh0oNyWIeedXDyaRYlYYE+k8o85xgL3eyngs88lLlTobkSNc1RYYEnFDqYkyKU3lIcqFBOYcLPPJSZQ6xhzv4cKUhyRmU6hmTYtQWklxYUL7KOXyLE4pK1wnJR2oJSc6gVMs7k9HVHpLFQu5QnsyzOZPHVLIftc8hptnJR2sNyWLeoVTDyWQ0UYUEwiaUhxYmlFWcUdm+1A6HmWYnH4smJOCEUhVjMrzoQpIzKKpDJyR/F1VIcgalfB5zDSfakCzW/8hrNd/BMzmDjRXuSik6zF6+wsejDMliHnmVx8lkcI0ICYROKHdxNo93QtHQDrOXO/n76EMCTihlMiaDaUxIciFBmeFug6KhdELyiUaEJGdQyuExV7jGhWSxkCOvb+cyj7wU7DB7+So3NCoki3nkVSwnkzCNDgmETij3cBaPYxXrKt6dmuYwe7mLGzjO0a5rYg4JOKEUzZj01/iQ5EKCss+gqI/DPNT4kOQMSnGMSW/JhCQXFpSvcRbns9Kg6BRHeIi72JpESHIGpRjemXSXXEgWC7lDeRJPYx3jFe5KMTvCQ9zNp5IKyWLeoYzGyWRpSYcEQieUezmL8z3y0kJIbkw2JOCEMipjcrrkQ5ILDcqZnOeRV4sdYYa7uZETCYckZ1CG5zHXo7UmJIv1P/JaxRP5UdZxboW7Ugw6IfmHVoRkMY+8BudkclIrQwIhE8oc+7mPM3msE0qLHGGGe/i/rQsJOKEMw5h0tDYkudCgnMF5rGRtxbtT1Y4ww9f4TCtDkjMogzEmhuQRIUE5sDChrDIoyTrCvtaHJGdQwrX9zsSQLCHkDuVb+WHWsqHCXakKR9jHvXyWE8x2XdOWkCzmHUp/bZ5MDEkXYRPK1zmDx3jklZCj7ONe/tGQLMEJpb+2xsSQ9BEalDMNShI6IfknQ9KDQemtjcdchmQA/Y68lrOKJ/CDrGWswl2pSEfZz318zpAE8shraW2bTAzJgMImlG9wBhudUBroKPu5n382JANwQllam2JiSIYUFpQHOINxg9IgnZD8iyEZgkE5XVuOuQxJAUKOvL6Fi1nL+gp3pWEcZT9f5yZDMiKPvE5qw2RiSAoSMqE8zDc5g3FWsKbi3SnUUQ7wDUNSCCeUk1KPiSEpWHhQzmWlQYlOJyQ3G5ICGZSOlGNiSEoSGpR1TihRmeUA3+AWQ1ICg5LunYkhqUD/O5SVPJ4fYA3nVLgrLaUTkn/lBMe6rjEko2vzHUqKk4khqUjYhPIg6zjXCaVGsxzgAT5vSCrQ5gkltZhkwBYMSWVCgnKQB1nLBoNSg1ke5puGpFIDBOWbwL9WurkSpXTMlWFIahNy5HU+E6zh7Ap31W6dkEwZkpoEHnm9GnhrdbsqTyqTSYYhqVXYhLJrYUJZXfHu2meWh3mQHYakRm2bUFKISYYhiUJIUA6xm7WMGZQSdULyBeYMSe3aFJSmxySjR0jGxsbYunWrIalQaFDWGZRSzHLQkESmLUFp8p1JhiGJVsgdymP5blZzVoW7StssB9nFrYYkUqnfoTR1MskwJFELm1D2sIb1TigFOMZBdvElQxKx1CeUJsYkw5A0QkhQDjPNWoMykk5IbjMkDZByUJp2zJVhSBon5MhrI09hNWdWuKs0HOMQu7mNOY53XWNI4pPikVeTJpMMQ9JIoRPKatazglUV7665jnGIPXzZkDRQihNKU2KSYUgarX9Q5jnCNGs4x6AE6ITkdkPSYKkFpQkxyTAkSQgLyl7WcA7LDUpXxzjENHcYkgSkFJTY70wyDEly+t+hrGCcJ7OKMyrcVTMc4zDT7DQkiUnhDiXmySTDkCQpbEJ5iNWc7YSyyDEOs5evGJIEpTChxBqTDEOSNIMymOMcZi93GpKENT0oMR5zZRiS1gg58trAt7X6yKsTkrsMSUs09cgrtskkw5C0StiEso/VnMVyVla8u/od5wgPGZJWaeqEElNMMgxJK/ULCi0NynGOMMPdhqSFmhiUWI65MgxJ6/U78lrGCjZwIStZV+Gu6tEJyT3McaLrGkOSviYdecUwmWQYEhE6oexnNWckPaEc5wj7+JohUaMmlLpjkmFItEhIUI5ygFWcmWRQjnOE/dxrSPSIpgSlzphkGBItITQoqU0oxzlqSLSkJgSlrjuTDEOiPvrfoSznHC5gJWsr3FU5OiG5n3lDoh5ivkOpYzLJMCQKEDKhzHKAlaxr9IRygqPs5+uGRH3FPKFUHZMMQ6IBhAXlYVZxBstrvwIcXCck3zAkChZrUKo85sowJBpSyJHX2TyOFaypcFejOcEsB3jAkGgosR15VfVXuQxDohGETSgHWclaljVgQjnBLA/zTUOiocU2oVTx1GUYEhUgJCjHOMgq1kZ95NUJyYOGRCOLKShlP3EZhkQFCgvKoWgnlBPMcpAHmWeu6xpDokHEEpQyn7YMQ6IShAdlDctZTufouP6PTkh2GRIVLoaglBWTDEOiEoUF5TArWBPFhHKCYxxityFRaeoOShlPWYYhUQVCgnKcw6xgda1BmeMYh9hjSFS6OoNS9BOWYUhUobCgHGEFq2s58ppjlkNMGxJVpq6gFBmTDEOiGgwSlGVLf3uWYm7hPRINiapWR1CKikmGIVGNYgvK3MJ7JBoS1aXqoBQRkwxDogj0Dwqc4AgrWMUyllHe0dYxjjBjSFS7KoMyakwyDIkiEhaUoyxnVSkTytzC7/81JIpFVUEZJSYZhkQRqisocxxnlv3MM991jSFRHaoIyrAxyTAkilhIUOaYZTkrCwnKHCcMiaJWdlCGiUmGIVEDhE0oeVCGv0OZ5zizHDAkil6ZQRk0JhmGRA0SFpRjC2+uNfg7MsxzgmMcNCRqjLKCMkhMMgyJGig0KCtYsTChhJlf+CUphkRNU0ZQQmOSYUjUYGFBOb7oV9f3O9o6wTEOL/zz0gyJYlZ0UEJikmFIlICwS/lOUHpNKPlPkhgSNV2RQekXkwxDooSEBeXEwiu8Tg/K/MJPkhgSpaKooPSKSYYhUYKGDcr8wguADYlSU0RQusUkw5AoYSFBmV8ISudFw3Oc4BiGRKkaNShLxSTDkKgFQoMCy5jjWM+v9eIXv5h3vOMdBe9QqtYoQTk1JhmGRC0SFpTuv2cLOhOJIVEqhg3K4phkGBK1UEhQuvFoSykaJih5TDIMiVpsmKAYEqVs0KAsw5BIj7jqqqvYsmVL33WGRG2xY8cOJicn2bNnT7cl88CrlwM/RJeQADzxiU/koosuKmGLUnxe+9rXsnr16p5r1q1bx+te97qKdiTV68ILL+SCCy7otWQZsHk58ErgLd1WTU1N8bSnPY29e/cWvEUpLjt27ODSSy9ldna257rDhw9zySWXcMstt1S0M6ke+/bt4/LLL2dqaqrXsmuBn8t/Ims58DbgZd1Wb968ma1btzI2NlbcTqVITE1NMTk5yfT0dPDnjI+Ps23bNiYmJkrcmVSPmZkZLr300n5/aboGeAUwlx9vzQFX0WNCueWWW5xQlKR8IhkkJADT09NOKEpSPpH0+d6+loWQwKPvSuaB19AjKNu3b2dyctKgKBmBl4sfgKV/2CTwb29SY+zbt4/LLruMm266qdeya+kMII88F6devBsUtUboq1SAK4GXY1CUuGFDAt3fWm4Z8GbgVd2+2qZNm9i2bRvnnnvugNuV6jdASN666N9l+DJ6JWqUkEDv9yk1KErSkCHJZRgUJWbUkED/N702KErKiCHJZRgUJaKIkED/mORrDIoar6CQ5DIMihquqJBAWEzydQZFjVVwSHIZBkUNVWRIIDwm+VqDosYpKSS5DIOihik6JDBYTPL1BkWNUXJIchkGRQ1RRkhg8Jjkn2NQlEudjgAABTJJREFUFL2KQpLLMCiKXFkhgeFikn+eQVG0Kg5JLsOgKFJlhgSGj0n+uQZF0akpJLkMg6LIlB0SGC0m+ecbFEWj5pDkMgyKIlFFSGD0mORfw6CodpGEJJdhUFSzqkICxcQk/zoGRbWJLCS5DIOimlQZkqItA/6azkO75MemTZvmp6en56UiTU1NzW/cuLHr9x2dB+Xnqn8kgE5QTnTb29jY2PzNN99c9/+FSszMzMz8xRdf3OuZmKfzxlZd37K9bgZFlYo8JLkMg6KKpBCSnEFRJRoSklyGQVHJUgpJzqCoVA0LSS7DoKgkKYYkZ1BUioaGJJdhUFSwlEOSMygqVMNDksswKCpIG0KSMygqRCIhyWUYFI2oTSHJGRSNJLGQ5DIMiobUxpDkDIqGkmhIchkGRQNqc0hyBkUDSTwkuQyDokCG5CSDoiAtCUkuw6CoD0NyOoOinloWklyGQVEXhqQ7g6IltTQkuQyDolMYkv4Mih6l5SHJZRgULTAk4QyK5ufnDckpMgxK6xmSwRmUljMkS8owKK1lSIZnUFrKkPSUYVBax5CMzqC0jCEJkmFQWsOQFMegtIQhGUiGQUmeISmeQUmcIRlKhkFJliEpj0FJlCEZSYZBSY4hKZ9BSYwhKUSGQUmGIamOQUmEISlUhkFpPENSPYPScIakFBkGpbEMSX0MSkMZklJlGJTGMST1MygNY0gqkWFQGsOQxMOgNIQhqVSGQYmeIYmPQYmcIalFhkGJliGJl0GJ1Pbt2w1JfTIMSnQMSfwMSmS2b98+Pz4+bkjqlWFQomFImsOgRMKQRCXDoNTOkDSPQamZIYlShkGpjSFpLoNSE0MStQyDUjlD0nwGpWKGpBEyDEplDEk6DEpFDEmjZBiU0hmS9BiUkhmSRsowKKUxJOkyKCUxJI2WYVAKZ0jSZ1AKZkiSkGFQCmNI2sOgFMSQJCXDoIzMkLSPQRmRIUlShkEZmiFpL4MyJEOStAyDMjBDIoMyIEPSChkGJZghUc6gBDIkrZJhUPoyJDqVQenDkLRShkHpypCoG4PShSFptQyDchpDon4MyikMiTAoj2JIFMqgLAgIyQngJdX/J1INXgAcp+VBMSQaVOuDYki0hFYHxZBoWK0NiiFRD60MiiHRqFoXFEOiAK0KiiFRUVoTFEOiAbQiKIZERUs+KIZEQ0g6KIZEZUk2KIZEI0gyKIZEZUsuKIZEBUgqKIZEVUkmKIZEBUoiKIZEVWt8UAyJStDooBgS1aWxQTEkKlEjg2JIVLfGBcWQqAKNCoohUSwaExRDogo1IiiGRLGJPiiGRDWIOiiGRLGKNiiGRDWKMiiGRLGLLiiGRBGIKiiGRE0RTVAMiSISRVAMiZqm9qAYEkWo1qAYEjVVbUExJIpYLUExJGq6yoNiSNQAlQbFkCgVlQXFkKhBKgmKIVFqSg+KIVEDlRoUQ6JUlRYUQ6IGKyUohkSpKzwohkQJKDQohkRtUVhQDIkSUkhQDInaZuSgGBIlaKSgGBK11dBBMSRK2FBBMSRqu4GDYkjUAgMFxZBIHcFBMSRqkaCgGBLp0ZYDW+jxQExMTISEJKt851J5Mjrf10t+z4+Pj89PTEz0C8kWDIlapu+E0uPDiUSp6jmh9PlwIlFr9Z1QlvhwIlHqMnpMKF0+nEjUeoNMKE4kaotBJhQnEmlByITiRKK2yeg/oTiRSKfoNaE4kaitek0oTiRSF0sFxZCo7ZYKiiGR+lgcFEMidSwOiiGRAi0H3oZ3JNJiGZ3nwpBE6P8DBZeM8E/tpV0AAAAASUVORK5CYII=" />
    <link rel="stylesheet" type="text/css" href="./styles.css" />
</head>
<body selected="0">
	<div id="ui">
		<svg id="stage" version="1.1" xmlns="http://www.w3.org/2000/svg">
			<defs>
				<lineargradient id="outline" x1="0%" y1="0%" x2="100%" y2="100%">
					<stop offset="0%" stop-color="#f00" />
					<stop offset="8.3333%" stop-color="#ff0" />
					<stop offset="16.6666%" stop-color="#0f0" />
					<stop offset="25%" stop-color="#0ff" />
					<stop offset="33.3333%" stop-color="#00f" />
					<stop offset="41.6666%" stop-color="#f0f" />
					<stop offset="50%" stop-color="#f00" />
					<stop offset="58.3333%" stop-color="#f0f" />
					<stop offset="66.6666%" stop-color="#00f" />
					<stop offset="75%" stop-color="#0ff" />
					<stop offset="83.3333%" stop-color="#0f0" />
					<stop offset="91.6666%" stop-color="#ff0" />
					<stop offset="100%" stop-color="#f00" />
				</lineargradient>
			</defs>
		</svg>
		<form id="ui-form" class="wrap" name="ui" action="" method="post" onSubmit="event.preventDefault();" spellcheck="false">
		
			<input type="submit" />
			
			<div id="temp_menu" class="item">
				<a class="selector menu" name="ui[menu]" alt="menu" href="javascript:void(0);" title="menu"></a>
				
				<input class="selector col row" type="image" name="ui[undo]" alt="undo" title="undo" src="./res/icon/undo.icon.svg" disabled="" />
				<input class="selector col" name="ui[redo]" type="image" alt="redo" title="redo" src="./res/icon/redo.icon.svg" disabled="" />
				
				<input class="selector" name="ui[save]" type="image" alt="save" title="save" src="./res/icon/save.icon.svg" />
			</div>
			<div id="layer_menu" selected="0" class="item">
				<table class="layers">
				<tr>
					<th class="name title">
						<input class="value" type="text" name="pname" id="pname" value="" placeholder="untitled.json" size="15" />
					</th>
				</tr>
				<tr>
					<td id="layer_menu_list" class="name desc">
						<p class="selector"><input class="value" type="text" name="ui[layer]" disabled="" value="Layer 1" /></p>
						
						<a href="javascript:void(0);" alt="layer menu handle" id="layer_button" class="overlay">
							<input type="text" name="ui[placeholder]" disabled="" value="" class="placeholder" alt="placeholder layer button" placeholder="Layer 1" />
						</a>
					</td>
				</tr>
				<tr>
					<td class="name foot">
						<a href="javascript:void(0);" alt="layer menu collapse button" id="layer_menu_button" class="overlay" title="collapse layer menu"></a>
					</td>
				</tr>
				</table>
			</div>
			<div id="resize_menu" class="item">
				<a href="javascript:void(0);" id="scale_button_a" class="selector begin" alt="pixel scale button up">decrease size</a>
				<a href="javascript:void(0);" id="scale_button2" class="bg" alt="pixel scale bar">select size</a>
				<a href="javascript:void(0);" class="selector" name="ui[scale]" alt="scale tool" alt="pixel scale slider button" id="scale_button">
					<input type="text" alt="pixel scale value" disabled="" placeholder="%" />
				</a>
				<a href="javascript:void(0);" id="scale_button_b" class="selector end" alt="pixel scale button down">increase size</a>
			</div>
			<a id="tool_menu" class="item">
				<input class="selector" type="image" name="ui[tool]" value="plot" alt="plot tool" src="./res/icon/plot.icon.svg" />
				<input class="selector" name="ui[tool]" type="image" value="select" alt="select tool" src="./res/icon/grid.icon.svg" />
			</a>
			<div id="swatches" class="item">
				<div class="container">
					<a class="selector" href="javascript:void(0);" alt="swatch anchor">swatch button</a>
					<img class="color" src="./res/clear.bg.png" alt="transparent placeholder" style="background-color: #999;" />
					<img class="color" src="./res/clear.bg.png" alt="transparent placeholder" style="background-color: #0ff;" />
					<img class="color" src="./res/clear.bg.png" alt="transparent placeholder" style="background-color: #f0f" />
					<img class="color" src="./res/clear.bg.png" alt="transparent placeholder" style="background-color: #ff0" />
					<img class="color" src="./res/clear.bg.png" alt="transparent placeholder" style="background-color: #fff" />
				</div>
			</div>
			<div id="color_menu" class="item">
				<input class="selector" type="color" name="ui[color]" alt="color picker" placeholder="#000" title="color" />
			</div>
			<div id="erase_menu" class="item">
				<div class="container">
					<img usemap="#ui[erase]" alt="erase map" src="./res/icon/erase.map.svg" />
					<span id="erase_menu_button" class="outline"></span>
					<map name="ui[erase]">
						<area shape="rect" alt="clear layer"  coords="0,0,40,40"    href="javascript:handleEraseMenu(0,40);" />
						<area shape="rect" alt="big eraser"   coords="0,40,40,80"   href="javascript:handleEraseMenu(3,40);" />
						<area shape="rect" alt="small eraser" coords="0,80,40,120"  href="javascript:handleEraseMenu(2,40);" />
						<area shape="rect" alt="span" 		  coords="0,120,40,160" href="javascript:handleEraseMenu(1,40);" />
					</map>
				</div>
			</div>
			<table id="popup_menu" class="item">
				<tr><th class="header"></th></tr>
				<tr>
					<td class="container">
						<textarea form="ui-form" id="save_menu_data" name="ui[data]" class="selector" placeholder="{JSON}"></textarea>
					</td>
				</tr>
			</table>
		</form>
	</div>
    <script type="text/javascript" src="./script.js"></script>
</body>
</html>
