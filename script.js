	
	var STAGE = document.getElementById("stage");
	var UI = document.getElementById("ui").children[1];
	var PNAME = document.getElementById("pname");
	
	var TMP_MENU_ITEMS = document.getElementById("temp_menu").children;
	var MENU_BUTTON = TMP_MENU_ITEMS[0];
	var UNDO_BUTTON = TMP_MENU_ITEMS[1];
	var REDO_BUTTON = TMP_MENU_ITEMS[2];
	var SAVE_BUTTON = TMP_MENU_ITEMS[3];
	
	var TOOL_MENU = document.getElementById("tool_menu");
	var COLOR_MENU = document.getElementById("color_menu").children[0];
	var ERASE_MENU_BUTTON = document.getElementById("erase_menu_button");
	var POPUP_MENU = document.getElementById("popup_menu");
	var SAVE_MENU_DATA = document.getElementById("save_menu_data");
	var SLIDER_BUTTON_A = document.getElementById("scale_button_a");
	var SLIDER_BUTTON_B = document.getElementById("scale_button_b");
	var RESIZE_MENU = document.getElementById("resize_menu");
	var SLIDER_BUTTON2 = document.getElementById("scale_button2");
	
	var SLIDER_BUTTON = document.getElementById("scale_button");
	var SLIDER = SLIDER_BUTTON.parentNode;
	
	var LAYER_MENU = document.getElementById("layer_menu");
	var LAYER_MENU_BUTTON = document.getElementById("layer_menu_button");
	
	var SWATCHES = document.getElementById("swatches");
	var SWATCH_MENU_ITEMS = SWATCHES.children[0].children;
	var SWATCH_BUTTON = SWATCH_MENU_ITEMS[0];
	
	var COLOR_HANDLE = true;
	var SWATCH_CHANGE = RESIZE = false;
	var PREV_SWATCH = -1;
	var SWATCH_RECT,SWATCH_LENGTH = SWATCH_MENU_ITEMS.length-1;
	var LAYER_MENU_ACTIVE = 0;
	var SVG_NS = STAGE.getAttribute('xmlns');
	var SLIDER_BUTTON_DATA = SLIDER_BUTTON.getBoundingClientRect();
	var SLIDER_POS,NOTCH_SIZE;
	var POPUP = 0;
	var CURSOR_OFFSET_QUOTIENT = 1;
	var ERASE = 0;
	var UI_STATE = 0;
	var P_SIZE = 50;
	var P_COLOR = '#000';
	var STATE = '';
	var NEXT_MOVE,PREV_MOVE = P_SIZE;
	var PIXEL,CURSOR = STAGE.insertBefore(document.createElementNS(SVG_NS,'rect'),null);
	var a, md,mx,my, x,y, xCount,yCount,slope, delta,dx,dy, abs_delta,abs_dx,abs_dy, sign_delta,signX,signY, offset_delta, td, half_p;
	var px = py = -P_SIZE;
	var i, j = 1;
	var TOOL = 0;
	var APP_DATA = {
		"name"     : "untitled",
		"size"     : P_SIZE,
		"color"    : P_COLOR,
		"swatches" : get_swatches(),
		"sig"      : "mr.emoron",
		"items"    : []
	};

	PNAME.value = APP_DATA.name;

	CURSOR.setAttribute("x",px);
	CURSOR.setAttribute("y",py);
	CURSOR.setAttribute("class","cursor");
	
	function get_swatches()
	{
		var ret = [];
		
		for (i=0; i<SWATCH_LENGTH; i++)
		{
			ret.push(SWATCH_MENU_ITEMS[i+1].style.backgroundColor);
		}
		return ret;
	}
	
	function get_swatch_rect()
	{
		var ret = SWATCH_BUTTON.getBoundingClientRect();
		
		return {
			"left"   : Math.floor(ret.left),
			"width"  : Math.ceil(ret.width / SWATCH_LENGTH)
		};
	}

	function get_slider_pos()
	{
		var ret = window.innerHeight > window.innerWidth ? "bottom" : "left";
		
		document.body.setAttribute("pos",ret);
		
		return ret;
	}

	function get_notch_size()
	{
		var data = SLIDER.getBoundingClientRect();
		var ret = SLIDER_POS === "bottom" ? data.width : data.height;
		
		return ret / 100;
	}

	function scale()
	{
		updateSlider();
		
		var d,ele,pixels = APP_DATA.items;
		var len = pixels.length;
		
		if (len === 2) {return;}
		
		for (var i=0; i<len; i++)
		{
			pixel = pixels[i];
			
			d = pixel.col;
			pixel.x = d*P_SIZE;
			
			d = pixel.row;
			pixel.y = d*P_SIZE;
			
			pixel.update();
		}
	}

	function ExoPixel(_x=-P_SIZE,_y=-P_SIZE,_color=P_COLOR)
	{
		this.element = STAGE.insertBefore(document.createElementNS(SVG_NS,"rect"),CURSOR);
		
		this.x = _x;
		this.y = _y;
		
		this.row = _y/P_SIZE;
		this.col = _x/P_SIZE;
		
		this.color = _color;
		
		this.update = function()
		{
			var ele = this.element;
			var px = this.x;
			var py = this.y;
			var color = this.color;
			
			this.row = py/P_SIZE;
			this.col = px/P_SIZE;
			
			ele.setAttribute("width",P_SIZE);
			ele.setAttribute("height",P_SIZE);
			
			ele.setAttribute("x",px);
			ele.setAttribute("y",py);
			
			ele.setAttribute("style","fill: "+color+"; outline: 1px solid "+color+";");
		};
		
		this.update_pos = function()
		{
			var ele = this.element;
			var px = this.x;
			var py = this.y;
			
			this.row = py/P_SIZE;
			this.col = px/P_SIZE;
			
			ele.setAttribute("x",px);
			ele.setAttribute("y",py);
		};
		
		this.update_color = function()
		{
			var ele = this.element;
			var color = this.color;
			
			ele.setAttribute("style","fill: "+color+"; outline: 1px solid "+color+";");
		};
		
		this.deletePixel = function(_start=-1)
		{
			var ele = this.element;
			
			ele.parentNode.removeChild(ele);
			if (_start !== -1) {APP_DATA.items.splice(_start,1);}
		};
		
		this.update();
	}

	function createPixel(_x,_y,_color=P_COLOR)
	{
		var pixel,items = APP_DATA.items;
		var len = items.length;
		
		for (a=0; a<len; a++)
		{
			pixel = items[a];
			
			if (pixel.x === _x && pixel.y === _y)
			{
				pixel.color = P_COLOR;
				pixel.update_color();
				
				return;
			}
		}

		return APP_DATA.items.push(new ExoPixel(_x,_y,_color));
	}

	function plotPixel()
	{
		var e = event.type == "touchstart" ? event.touches[0] : event;
		
		if (e.which == 1 || event.type == "touchstart")
		{
			mx = e.clientX;
			my = e.clientY;
			
			px = mx - (mx % P_SIZE);
			py = my - (my % P_SIZE);
			
			if (event.type == "touchstart" && event.touches.length > 1)
			{
				erasePixel(px,py);
				toggleMenu();
				
				return;
			}
			else if (!TOOL) {createPixel(px,py);}
			else if (ERASE) {erasePixel(px,py);}
			
			STATE = "DRAG";
		}
	}

	function erasePixel(_x,_y)
	{
		var pixel,items = APP_DATA.items;
		var len = items.length;
		
		if (CURSOR_OFFSET_QUOTIENT == 2)
		{
			for (i=0; i<len; i++)
			{
				pixel = items[i];
				
				if (pixel.x == _x-P_SIZE && pixel.y == _y-P_SIZE
				 || pixel.x == _x        && pixel.y == _y-P_SIZE
				 || pixel.x == _x+P_SIZE && pixel.y == _y-P_SIZE
				 
				 || pixel.x == _x-P_SIZE && pixel.y == _y+P_SIZE
				 || pixel.x == _x        && pixel.y == _y+P_SIZE
				 || pixel.x == _x+P_SIZE && pixel.y == _y+P_SIZE
				 
				 || pixel.x == _x-P_SIZE && pixel.y == _y
				 || pixel.x == _x        && pixel.y == _y
				 || pixel.x == _x+P_SIZE && pixel.y == _y)
				{
					pixel.deletePixel(i);
					len--;
					i=-1;
				}
			}
			
			return;
		}
		
		for (i=0; i<len; i++)
		{			
			pixel = items[i];
			
			if (pixel.x == _x && pixel.y == _y)
			{
				pixel.deletePixel(i);
				
				break;
			}
		}
	}

	function translatePixels(_dx,_dy)
	{
		var item,len = APP_DATA.items.length;
		
		for (i=0; i<len; i++)
		{
			item = APP_DATA.items[i];
			item.x += _dx;
			item.y += _dy;
			item.update_pos();
		}
	}

	function update()
	{
		SLIDER_POS = get_slider_pos();
		NOTCH_SIZE = get_notch_size();
		
		scale();
		
		SWATCH_RECT = get_swatch_rect();
	}

	function move()
	{
		var e = event.type == "touchmove" ? event.touches[0] : event;
		
		mx = e.clientX;
		my = e.clientY;
		
		if (RESIZE)
		{
			half_p = P_SIZE/2;
			CURSOR.setAttribute("x",(window.innerWidth/2) - half_p);
			CURSOR.setAttribute("y",(window.innerHeight/2) - half_p);
			
			md = SLIDER_POS === "left" ? my : mx;
			delta = md - td;
			abs_delta = Math.abs(delta);
			sign_delta = delta < 0 ? -1 : 1;
			
			if (abs_delta >= NOTCH_SIZE)
			{
				NEXT_MOVE = PREV_MOVE + (sign_delta * Math.floor(abs_delta / NOTCH_SIZE));
				
				if (NEXT_MOVE > 0 && NEXT_MOVE <= 100)
				{
					P_SIZE = PREV_MOVE = NEXT_MOVE;
					offset_delta = delta % NOTCH_SIZE;
					td = md - offset_delta;
					
					scale();
				}
			}
			
			return;
		}
		
		x = mx - (mx % P_SIZE);
		y = my - (my % P_SIZE);
		
		if (TOOL && ERASE && CURSOR_OFFSET_QUOTIENT == 2)
		{
			CURSOR.setAttribute("x",mx-P_SIZE);
			CURSOR.setAttribute("y",my-P_SIZE);
			
			if (STATE === "DRAG")
			{
				erasePixel(x,y);
			}
			
			px = x;
			py = y;
			j=1;
			
			return;
		}
		
		dx = x - px;
		dy = y - py;
		
		abs_dx = dx < 0 ? -dx : dx;
		abs_dy = dy < 0 ? -dy : dy;
		
		if (abs_dx >= P_SIZE || abs_dy >= P_SIZE)
		{
			if (STATE === "DRAG")
			{
				if (TOOL && !ERASE)
				{
					px = x;
					py = y;
					j=1;
					
					return translatePixels(dx,dy);
				}
				
				signX = dx ? dx / abs_dx : 0;
				signY = dy ? dy / abs_dy : 0;
				
				xCount = abs_dx / P_SIZE;
				yCount = abs_dy / P_SIZE;
				
				if (xCount > yCount && yCount)
				{
					for (i=1; i<xCount; i++)
					{
						if (( i * (yCount/xCount) ) + 0.5 > j)
						{
							py += signY*P_SIZE;
							j++;
						}
						
						px += signX*P_SIZE;
						
						if (!TOOL) {createPixel(px,py);}
						else if (ERASE) {erasePixel(px,py);}
					}
				}
				else if (yCount > xCount && xCount)
				{
					for (i=1; i<yCount; i++)
					{
						if (( i * (xCount/yCount) ) + 0.5 > j)
						{
							px += signX*P_SIZE;
							j++;
						}
						
						py += signY*P_SIZE;
						
						if (!TOOL) {createPixel(px,py);}
						else if (ERASE) {erasePixel(px,py);}
					}
				}
				else if (xCount > 1)
				{
					for (i=1; i<xCount; i++)
					{
						px += signX*P_SIZE;
						py += signY*P_SIZE;
						
						if (!TOOL) {createPixel(px,py);}
						else if (ERASE) {erasePixel(px,py);}
					}
				}
				else if (yCount > 1)
				{
					for (i=1; i<yCount; i++)
					{
						px += signX*P_SIZE;
						py += signY*P_SIZE;
						
						if (!TOOL) {createPixel(px,py);}
						else if (ERASE) {erasePixel(px,py);}
					}
				}
				
				if (!TOOL) {createPixel(x,y);}
				else if (ERASE) {erasePixel(x,y);}
			}
			
			CURSOR.setAttribute('x',x);
			CURSOR.setAttribute('y',y);
		}
		
		px = x;
		py = y;
		j=1;
	}

	function clear()
	{
		event.preventDefault();
		
		if (event.which == 1 || event.type !== "mouseup")
		{
			RESIZE = false;
		}
		
		STATE = "";
	}
		
	function updateSlider()
	{
		var str = P_SIZE + "%";
		
		updateCursor();
		
		SLIDER_BUTTON.children[0].value = str;
		
		if (SLIDER_POS === "left")
		{
			SLIDER_BUTTON.style.top = str;
			SLIDER_BUTTON.style.left = "";
		}
		else
		{
			SLIDER_BUTTON.style.top = "";
			SLIDER_BUTTON.style.left = str;
		}
	}

	function updateCursor()
	{
		var d = TOOL ? CURSOR_OFFSET_QUOTIENT : 1;
		
		CURSOR.setAttribute("width",P_SIZE*d);
		CURSOR.setAttribute("height",P_SIZE*d);
	}

	function handleResize()
	{
		event.preventDefault();
		
		td = SLIDER_POS === "left" ? event.clientY : event.clientX;
		
		half_p = (P_SIZE/2)*CURSOR_OFFSET_QUOTIENT;
		CURSOR.setAttribute("x",(window.innerWidth/2) - half_p);
		CURSOR.setAttribute("y",(window.innerHeight/2) - half_p);
		
		RESIZE = true;
	}

	function handleResize2()
	{
		event.preventDefault();
		
		var data = SLIDER.getBoundingClientRect();
		var m,p,d,q;
		
		if (SLIDER_POS === "left")
		{
			m = event.clientY;
			p = data.top;
			d = data.height;
		}
		else
		{
			m = event.clientX;
			p = data.left;
			d = data.width;
		}
		
		q = m-p;
		
		if (q <= 0) {q = 1; td = p;}
		else if (m > p+d) {q = 100; td = p+d;}
		else {q = Math.round(q / NOTCH_SIZE); td = m;}
		
		PREV_MOVE = P_SIZE = q;
		
		half_p = (q/2)*CURSOR_OFFSET_QUOTIENT;
		CURSOR.setAttribute("x",(window.innerWidth/2) - half_p);
		CURSOR.setAttribute("y",(window.innerHeight/2) - half_p);
		
		scale();
	}

	function handleResize3(a)
	{
		var sign = a ? -1 : 1;
		
		P_SIZE += sign;
		
		if (P_SIZE < 1)
		{
			P_SIZE = 1;
		}
		else if (P_SIZE > 100)
		{
			P_SIZE = 100;
		}
		
		PREV_MOVE = P_SIZE;
		
		half_p = P_SIZE/2;
		CURSOR.setAttribute("x",(window.innerWidth/2) - half_p);
		CURSOR.setAttribute("y",(window.innerHeight/2) - half_p);
		
		scale();
	}

	function toggleMenu()
	{
		if (POPUP) {return;}
		
		event.preventDefault();
		
		UI_STATE = +!UI_STATE;
		UI.style.display = UI_STATE ? "block" : "";
	}

	function loadWindow()
	{
		toggleMenu();
		
		SLIDER_POS = get_slider_pos();
		NOTCH_SIZE = get_notch_size();
		
		saveData();
		updateSlider();
		
		SWATCH_RECT = get_swatch_rect();
	}

	function saveData()
	{
		var data = {
			"name"     : APP_DATA.name,
			"sig"      : APP_DATA.sig,
			"size"     : P_SIZE,
			"color"    : P_COLOR,
			"swatches" : [],
			"items"    : []
		};
		var item,items = APP_DATA.items;
		var len = items.length;
		var idata,arr = [];
		var swatch_str = '["'+APP_DATA.swatches.join('",\n        "')+'"]';
		
		if (data.name == "") {data.name = "untitled";}
		
		data.name = data.name.replace(/.json/gm,"") + ".json";
		PNAME.value = APP_DATA.name = data.name;
		
		for (i=0; i<len; i++)
		{
			item = items[i];
			arr.push({"x":item.x, "y":item.y, "color":item.color});
		}
		
		idata = JSON.stringify(arr)        			.replace(/},/gm,"},\n        ");
		
		SAVE_MENU_DATA.value = JSON.stringify(data)	.replace("{","{\n    ")
													.replace(/}$/,"\n}")
													.replace(/,"/gm,',\n    "')
													.replace("[]",swatch_str)
													.replace("[]",idata)
													.replace(/\[/gm,"[\n        ")
													.replace(/\]/gm,"\n    ]")
													.replace(/:/gm," : ");
	}

	function loadData(_data)
	{
		var pItems = APP_DATA.items;
		var itemA,itemB,nItems = _data.items;
		var plen = pItems.length;
		var nlen = nItems.length;
		var size = PREV_MOVE = P_SIZE = APP_DATA.size = _data.size;
		var swatches = _data.swatches;
		
		//remove duplicates and errors
		for (i=0; i<nlen; i++)
		{
			itemA = nItems[i];
			
			for (a=0; a<nlen; a++)
			{
				if (i == a) {continue;}
				
				itemB = nItems[a];
				
				if (itemB.x % size || itemB.y % size ||
					itemA.x == itemB.x && itemA.y == itemB.y)
				{
					nItems.splice(a,1);
					nlen--;
					a--;
				}
			}
		}
		
		var len = plen > nlen ? plen : nlen;
		
		for (i=0; i<len; i++)
		{
			itemA = pItems[i];
			itemB = nItems[i];
			
			if (itemB == undefined) {itemA.deletePixel(i); len--; i--;}
			else if (itemA == undefined) {itemA = createPixel(itemB.x,itemB.y,itemB.color);}
			else
			{
				itemA.x = itemB.x;
				itemA.y = itemB.y;
				itemA.color = itemB.color;
				itemA.update();
			}
		}
		
		for (i=0; i<SWATCH_LENGTH; i++)
		{
			SWATCH_MENU_ITEMS[i+1].style.backgroundColor = swatches[i];
		}
		
		APP_DATA.name = _data.name;
		APP_DATA.sig = _data.sig;
		APP_DATA.color = P_COLOR = _data.color;
		CURSOR.setAttribute("style","fill: "+P_COLOR);
		
		updateSlider();
		saveData();
	}

	function handleTool()
	{
		TOOL = +!TOOL;
		
		updateCursor();
		
		document.body.setAttribute("selected",TOOL);
	}

	function update_history()
	{
		HISTORY.push(get_data());
	}

	function undo_redo(e,undo=1)
	{
		console.log(undo);
		if (undo)
		{
			
		}
		else
		{
			
		}
	}

	function handleColor()
	{
		var target = event.currentTarget;
		
		if (!COLOR_HANDLE && event.type == "click")
		{
			event.preventDefault();
			COLOR_HANDLE = true;
		}
		else {COLOR_HANDLE = false;}
		
		P_COLOR = target.value;
		CURSOR.setAttribute("style","fill: "+P_COLOR);
		
		if (SWATCH_CHANGE)
		{
			SWATCH_MENU_ITEMS[PREV_SWATCH+1].style.backgroundColor = P_COLOR;
		}
		else
		{
			PREV_SWATCH = -1;
		}
	}

	function handleEraseMenu(_item,_width)
	{
		if (_item)
		{
			ERASE_MENU_BUTTON.style.bottom = ((_item-1)*_width)+"px";
			ERASE = _item-1;
			
			document.body.setAttribute("erase",ERASE);
			CURSOR_OFFSET_QUOTIENT = (ERASE == 2) ? 2 : 1;
			
			CURSOR.setAttribute("width",P_SIZE*CURSOR_OFFSET_QUOTIENT);
			CURSOR.setAttribute("height",P_SIZE*CURSOR_OFFSET_QUOTIENT);
		}
		else
		{
			var pixels = APP_DATA.items;
			var len = pixels.length;
			
			for (i=0; i<len;)
			{
				pixels[i].deletePixel(i);
				len--;
			}
		}
	}

	function handleSaveMenu()
	{
		event.preventDefault();
		
		if (!POPUP) {saveData(); document.body.setAttribute("save_menu","1");}
		else if (!testLoad()) {return;}
		else {document.body.setAttribute("save_menu","0");}
		
		POPUP = +!POPUP;
	}

	function testLoad()
	{
		var ret = true;
		var d = JSON.parse(SAVE_MENU_DATA.value);
		
		try {loadData(d);}
		catch(e) {
			ret = false;
			
			window.alert("Typo Alert:\n\n"+e.message);
		}
		
		return ret;
	}

	function download()
	{
		if (!testLoad()) {return;}
		
		const link = document.createElement("a");
		const content = SAVE_MENU_DATA.value;
		const file = new Blob([content],{type:"application.json"});
		
		link.href = URL.createObjectURL(file);
		link.download = APP_DATA.name.replace(".json","") + ".json";
		link.click();
		URL.revokeObjectURL(link.href);
	}
	
	function handleLayerMenu()
	{
		LAYER_MENU_ACTIVE = +!LAYER_MENU_ACTIVE;
		LAYER_MENU.setAttribute("selected",LAYER_MENU_ACTIVE);
	}
	
	function handleSubmit()
	{
		event.preventDefault();

		PNAME.blur();
		APP_DATA.name = PNAME.value;
		
		saveData();
	}
	
	function handleSwatchMenu()
	{
		var active,tx = event.clientX;
		var left = SWATCH_RECT.left;
		var width = SWATCH_RECT.width;
		
		for (i=0; i<SWATCH_LENGTH; i++)
		{
			if (tx >= left + (i * width) && tx < left + ((i+1) * width))
			{
				active = 0;
				COLOR_HANDLE = true;
				
				if (PREV_SWATCH == i)
				{
					if (!SWATCH_CHANGE)
					{
						active = i+1;
						SWATCH_CHANGE = true;
					}
					else
					{
						SWATCH_CHANGE = false;
					}
				}
				else
				{
					P_COLOR = SWATCH_MENU_ITEMS[i+1].style.backgroundColor;
					CURSOR.setAttribute("style","fill: "+P_COLOR);
					SWATCH_CHANGE = false;
				}
				
				SWATCHES.setAttribute("active",active);
				PREV_SWATCH = i;
				
				break;
			}
		}
	}
	
	SWATCH_BUTTON    .addEventListener("click",handleSwatchMenu);
	LAYER_MENU_BUTTON.addEventListener("click",handleLayerMenu);
	SAVE_BUTTON      .addEventListener("click",download);
	COLOR_MENU       .addEventListener("click",handleColor);
	COLOR_MENU       .addEventListener("input",handleColor);
	COLOR_MENU       .addEventListener("change",handleColor);
	TOOL_MENU        .addEventListener("click",handleTool);
	MENU_BUTTON      .addEventListener("click",handleSaveMenu);
	UNDO_BUTTON      .addEventListener("click",function(e){undo_redo(e,1);});
	REDO_BUTTON      .addEventListener("click",function(e){undo_redo(e,0);});
	SLIDER_BUTTON    .addEventListener("mousedown",handleResize);
	SLIDER_BUTTON2   .addEventListener("mousedown",function(e){e.preventDefault();});
	SLIDER_BUTTON2   .addEventListener("click",handleResize2);
	SLIDER_BUTTON_A  .addEventListener("click",function(e){e.preventDefault(); handleResize3(1);});
	SLIDER_BUTTON_B  .addEventListener("click",function(e){e.preventDefault(); handleResize3(0);});
	STAGE            .addEventListener("touchstart",plotPixel);
	STAGE            .addEventListener("mousedown",plotPixel);
	window           .addEventListener("mouseup",clear);
	window           .addEventListener("touchend",clear);
	window           .addEventListener("touchcancel",clear);
	window           .addEventListener("mousemove",move);
	window           .addEventListener("resize",update);
	window           .addEventListener("contextmenu",toggleMenu);
	document.forms["ui"]
				     .addEventListener("submit",handleSubmit);
	window           .addEventListener("load",loadWindow);
	window		     .addEventListener("touchstart",function(e){e.preventDefault();},{passive:false});
	window		     .addEventListener("touchmove",move);
